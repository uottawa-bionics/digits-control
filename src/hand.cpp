/*
* Describes the entire hand. Consists of 5 digits. Hand movement profiles can be found here
*/
#include "Arduino.h"
#include "util.h"
#include "hand.h"
#include "digit.h"

Hand::Hand(int pins[5], int ampPin[5], int posPin[5], int potentioPin[5])
{
    _index = Digit(pins[0], ampPin[0], posPin[0], potentioPin[0]);
    _middle = Digit(pins[1], ampPin[1], posPin[1], potentioPin[1]);
    _ring = Digit(pins[2], ampPin[2], posPin[2], potentioPin[2]);
    _small = Digit(pins[3], ampPin[3], posPin[3], potentioPin[3]);
    _thumb = Digit(pins[4], ampPin[4], posPin[4], potentioPin[4]);
    Serial.println("Hand initalized");
}
Hand::Hand(int pins[5], int ampPin[5], int posPin[5], int potentioPin[5], int currentPos[5])
{
    _index = Digit(pins[0], ampPin[0], posPin[0], potentioPin[0], currentPos[0]);
    _middle = Digit(pins[1], ampPin[1], posPin[1], potentioPin[1], currentPos[1]);
    _ring = Digit(pins[2], ampPin[2], posPin[2], potentioPin[2], currentPos[2]);
    _small = Digit(pins[3], ampPin[3], posPin[3], potentioPin[3], currentPos[3]);
    _thumb = Digit(pins[4], ampPin[4], posPin[4], potentioPin[4], currentPos[4]);
    Serial.println("Hand initalized");
}

void Hand::moveIndex(int targetPos, int motionProfile, int moveDuration)
{
    _index.moveTo(targetPos, motionProfile, moveDuration);
}
void Hand::moveMiddle(int targetPos, int motionProfile, int moveDuration)
{
    _middle.moveTo(targetPos, motionProfile, moveDuration);
}
void Hand::moveRing(int targetPos, int motionProfile, int moveDuration)
{
    _ring.moveTo(targetPos, motionProfile, moveDuration);
}
void Hand::moveSmall(int targetPos, int motionProfile, int moveDuration)
{
    _small.moveTo(targetPos, motionProfile, moveDuration);
}
void Hand::moveSmall(int targetPos, int motionProfile, int moveDuration)
{
    _thumb.moveTo(targetPos, motionProfile, moveDuration);
}

void Hand::tripodPinch()
{
}

void Hand::figerTipTouch()
{
}

void Hand::stop()
{
    _index.stop();
    _middle.stop();
    _ring.stop();
    _small.stop();
}