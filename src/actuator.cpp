/* actuator.h
 * Description: Functions for moving the PQ12 linear actuators
 */

#include "Arduino.h"
#include "util.h"
#include "actuator.h"

LinearActuator::LinearActuator(int pin, int currentPos) : m_pin(pin), m_currentPos(currentPos), m_targetPos(currentPos)
{
    pq.attach(m_pin);
    pq.writeMicroseconds(currentPos);
}

// Configures info about servo to generate the motion profile and keep track of movement
void LinearActuator::moveTo(unsigned int targetPos, int motionProfile, int moveDuration)
{
    //if(targetPos > PWM_MAX || targetPos < PWM_MIN) return;
    m_stop = false;
    m_moveDuration = moveDuration;
    m_currentms = 0;
    m_targetPos = targetPos;
    m_motionProfile = motionProfile;
    m_startPos = m_currentPos;
}

void LinearActuator::update()
{
    // Check if stop() was called
    if (!m_stop)
    {
        // Stop if finished moving based on time
        if (m_currentms >= m_moveDuration)
        {
            stop();
        }
        else
        {
            m_currentms += 10;

            switch (m_motionProfile)
            {
            case 1:
                m_currentPos = linear();
                break;
            case 2:
                m_currentPos = easeOutCubic();
                break;
            default:
                break;
            }
            pq.writeMicroseconds(m_currentPos);
            //Serial.println(m_currentPos);
        }
    }
}
// Function used to stop movement in update()
void LinearActuator::stop()
{
    m_stop = true;
}
int LinearActuator::getCurrentPos()
{
    return m_currentPos;
}
void LinearActuator::setTargetPos(int target)
{
    m_targetPos = target;
}
int LinearActuator::getTargetPos()
{
    return m_targetPos;
}
int LinearActuator::linear()
{
    int resultPos = ((m_targetPos - m_startPos) / float(m_moveDuration)) * m_currentms + m_startPos;
    return resultPos;
}
int LinearActuator::easeOutCubic()
{
    int resultPos = (m_targetPos - m_startPos) * (1 - pow((1 - (1 / float(m_moveDuration)) * m_currentms), 3)) + m_startPos;
    return resultPos;
}