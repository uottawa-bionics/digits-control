#ifndef hand_h
#define hand_h

#include "Arduino.h"
#include "util.h"
#include "digit.h"

class Hand
{
private:
    Digit _index;
    Digit _middle;
    Digit _ring;
    Digit _small;
    Digit _thumb;

public:
    Hand(int pins[5], int ampPin[5], int posPin[5], int potentioPin[5]);
    Hand(int pins[5], int ampPin[5], int posPin[5], int potentioPin[5], int currentPos[5]);

    //If this is too cluttered, can make it one function with an int to select finger (Ex. 0-thumb, 1-index...)
    void moveIndex(int targetPos, int motionProfile, int moveDuration);
    void moveMiddle(int targetPos, int motionProfile, int moveDuration);
    void moveRing(int targetPos, int motionProfile, int moveDuration);
    void moveSmall(int targetPos, int motionProfile, int moveDuration);
    void moveThumb(int targetPos, int motionProfile, int moveDuration);
    void tripodPinch();
    void figerTipTouch();
    void stop();
    void update();
};

#endif