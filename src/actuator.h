/* actuator.h
 * Description: Functions for moving the PQ12 linear actuators
 */
#ifndef actuator_h
#define actuator_h

#include "Arduino.h"
#include "util.h"
#include <Servo.h>

#define PWM_MIN 544
#define PWM_MAX 2400

class LinearActuator
{
private:
    int m_pin;              // GPIO pin
    int m_currentPos = 544; //keeps track of updated position
    int m_targetPos;        //keeps track of the destination
    int m_startPos = 544;   //keeps track of where the servo started when move() was called, used for motion curve generation
    bool m_stop = true;
    // 1 - linear, 2 - ease out cubic
    int m_motionProfile;
    int m_currentms = 0; // relative time from when moveTo() was called that is used to calculate next position to move to
    int m_moveDuration;  // Total time to get to target position
    Servo pq;            // PQ12 servo

public:
    LinearActuator();
    LinearActuator(int pin, int currentPos);
    // check if stop else move actuator based on motionprofile function
    void update();
    // toggle stop to true and determine
    void moveTo(unsigned int targetPos, int motionProfile, int moveDuration);
    // for immediate stoping of the actuator
    void stop();

    int getCurrentPos();
    void setTargetPos(int target);
    int getTargetPos();
    int linear();
    int easeOutCubic();
};

#endif