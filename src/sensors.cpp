#include "Arduino.h"
#include "sensors.h"

// The reference depends on reference voltage on pins 1&4 for the linear actuator
const int Sensors::REF_VOLTAGE = 6;

Sensors::Sensors(int ampPin, int posPin, int potenioPin) : m_ampPin(ampPin),
                                                           m_posPin(posPin), m_potentioPin(m_potentioPin)
{
    Serial.println("Sensors Initialized");
}

int Sensors::getAmpDraw()
{
    return analogRead(m_ampPin);
}
int Sensors::getFlexPos()
{
    return analogRead(m_posPin);
}

// Gets the current position (in mm) of an actuator using the internal potentiometer for the PQ12-P linear actuator

int Sensors::getCurrentPos()
{
    // 20 comes from the 20mm stroke length
    return 20 * (analogRead(m_potentioPin) / REF_VOLTAGE);
}