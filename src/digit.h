#ifndef digit_h
#define digit_h

#include "Arduino.h"
#include <Servo.h>
#include "sensors.h"
#include "actuator.h"

class Digit : public LinearActuator, public Sensors
{
private:
    Sensors sensor;
    LinearActuator actuator;
    int m_pin; // servo pin
    float m_target_pos;
    float m_current_pos;
    int stepSpeed; // pwm ms increase per step

public:
    Digit();
    Digit(int pin, int ampPin, int posPin, int potentioPin);
    Digit(int pin, int ampPin, int posPin, int potentioPin, int currentPos);
    bool atTargetPos();
    void incrementPos(); // move servo one step closer to target position
    void setTargetPos(float position);
};

#endif