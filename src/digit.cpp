/* 
 * Describes a single digit which consists of a linear actuator and sensors
 */
#include "Arduino.h"
#include "util.h"
#include "math.h"
#include "sensors.h"
#include "digit.h"

Digit::Digit(int pin, int ampPin, int posPin, int potentioPin)
{
    actuator = LinearActuator(pin, 0); //Defaults to currnet position 0 when first instaniated
    sensor = Sensors(ampPin, posPin, potentioPin);
};
Digit::Digit(int pin, int ampPin, int posPin, int potentioPin, int currentPos)
{
    actuator = LinearActuator(pin, currentPos);
    sensor = Sensors(ampPin, posPin, potentioPin);
};

//Checks if the actuator is at the target position
boolean Digit::atTargetPos()
{
    return actuator.getCurrentPos() == m_target_pos;
}

//Increments the current position
//This is really inefficent way of moving
void Digit::incrementPos()
{
    int i = actuator.getCurrentPos() + 1;
    actuator.setTargetPos(i);
    actuator.update();
}
//Changes the target position
void Digit::setTargetPos(float position)
{
    actuator.setTargetPos(position);
}
