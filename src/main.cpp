#include "actuator.h"
#include "digit.h"
LinearActuator d1(0, 700);

void setup()
{
  Serial.begin(115200);
  delay(100);
  Serial.println("Starting!");
  delay(1000);

  d1.moveTo(2000, 2, 700);
}

void loop()
{
  d1.update();
  delay(10);
}