#ifndef sensors_h
#define sensors_h

#include "Arduino.h"

class Sensors
{
private:
    int m_ampPin, m_posPin, m_potentioPin;

public:
    static const int REF_VOLTAGE;
    Sensors();
    Sensors(int ampPin, int posPin, int potentioPin);
    int getAmpDraw();
    int getFlexPos();
    int getCurrentPos();
};

#endif